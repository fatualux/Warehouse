## WAREHOUSE

Private VideoArt Project by Francesco Zubani and Nicola Ballarini

The material in this repository is produced using DML software, but there is a lot of human work behind, and, in some cases, it is registered under our names: so, if you want to use it, please contact us.

## LICENSE

[![License](https://img.shields.io/badge/License-BSD%20v4-blue.svg)](https://spdx.org/licenses/BSD-4-Clause)

This project is licensed under the BSDv4 license.
See LICENSE file for more details.
