## AUTHOR

- [Francesco Zubani](https://www.linkedin.com/in/francesco-zubani-5957081a6/)
- [Nicola Ballarini](https://www.instagram.com/odinorunegraf/)

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [Stable Diffusion](https://github.com/CompVis/stable-diffusion) - [LICENSE](https://github.com/CompVis/stable-diffusion/blob/main/LICENSE)
- [ComfyUI](https://github.com/comfyanonymous/ComfyUI) - [LICENSE](https://github.com/comfyanonymous/ComfyUI/blob/master/LICENSE)
- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)

These images are based on the work of Nicola Ballarini and post processed by Francesco Zubani.

## GALLERY

### Tim, Final

<div class="gallery">
  <a href="Tim_Final_001.png"><img class="thumbnail" src="thumbs/Tim_Final_001.png" alt="Tim_Final_001"></a>
  <a href="Tim_Final_002.png"><img class="thumbnail" src="thumbs/Tim_Final_002.png" alt="Tim_Final_002"></a>
  <a href="Tim_Final_003.png"><img class="thumbnail" src="thumbs/Tim_Final_003.png" alt="Tim_Final_003"></a>
  <a href="Tim_Final_004.png"><img class="thumbnail" src="thumbs/Tim_Final_004.png" alt="Tim_Final_004"></a>
  <a href="Tim_Final_005.png"><img class="thumbnail" src="thumbs/Tim_Final_005.png" alt="Tim_Final_005"></a>
  <a href="Tim_Final_006.png"><img class="thumbnail" src="thumbs/Tim_Final_006.png" alt="Tim_Final_006"></a>
  <a href="Tim_Final_007.png"><img class="thumbnail" src="thumbs/Tim_Final_007.png" alt="Tim_Final_007"></a>
  <a href="Tim_Final_008.png"><img class="thumbnail" src="thumbs/Tim_Final_008.png" alt="Tim_Final_008"></a>
  <a href="Tim_Final_009.png"><img class="thumbnail" src="thumbs/Tim_Final_009.png" alt="Tim_Final_009"></a>
  <a href="Tim_Final_010.png"><img class="thumbnail" src="thumbs/Tim_Final_010.png" alt="Tim_Final_010"></a>
  <a href="Tim_Final_011.png"><img class="thumbnail" src="thumbs/Tim_Final_011.png" alt="Tim_Final_011"></a>
  <a href="Tim_Final_012.png"><img class="thumbnail" src="thumbs/Tim_Final_012.png" alt="Tim_Final_012"></a>
  <a href="Tim_Final_013.png"><img class="thumbnail" src="thumbs/Tim_Final_013.png" alt="Tim_Final_013"></a>
  <a href="Tim_Final_014.png"><img class="thumbnail" src="thumbs/Tim_Final_014.png" alt="Tim_Final_014"></a>
  <a href="Tim_Final_015.png"><img class="thumbnail" src="thumbs/Tim_Final_015.png" alt="Tim_Final_015"></a>
  <a href="Tim_Final_016.png"><img class="thumbnail" src="thumbs/Tim_Final_016.png" alt="Tim_Final_016"></a>
  <a href="Tim_Final_017.png"><img class="thumbnail" src="thumbs/Tim_Final_017.png" alt="Tim_Final_017"></a>
  <a href="Tim_Final_018.png"><img class="thumbnail" src="thumbs/Tim_Final_018.png" alt="Tim_Final_018"></a>
  <a href="Tim_Final_019.png"><img class="thumbnail" src="thumbs/Tim_Final_019.png" alt="Tim_Final_019"></a>
  <a href="Tim_Final_020.png"><img class="thumbnail" src="thumbs/Tim_Final_020.png" alt="Tim_Final_020"></a>
  <a href="Tim_Final_021.png"><img class="thumbnail" src="thumbs/Tim_Final_021.png" alt="Tim_Final_021"></a>
  <a href="Tim_Final_022.png"><img class="thumbnail" src="thumbs/Tim_Final_022.png" alt="Tim_Final_022"></a>
  <a href="Tim_Final_023.png"><img class="thumbnail" src="thumbs/Tim_Final_023.png" alt="Tim_Final_023"></a>
  <a href="Tim_Final_024.png"><img class="thumbnail" src="thumbs/Tim_Final_024.png" alt="Tim_Final_024"></a>
  <a href="Tim_Final_025.png"><img class="thumbnail" src="thumbs/Tim_Final_025.png" alt="Tim_Final_025"></a>
  <a href="Tim_Final_026.png"><img class="thumbnail" src="thumbs/Tim_Final_026.png" alt="Tim_Final_026"></a>
  <a href="Tim_Final_027.png"><img class="thumbnail" src="thumbs/Tim_Final_027.png" alt="Tim_Final_027"></a>
  <a href="Tim_Final_028.png"><img class="thumbnail" src="thumbs/Tim_Final_028.png" alt="Tim_Final_028"></a>
  <a href="Tim_Final_029.png"><img class="thumbnail" src="thumbs/Tim_Final_029.png" alt="Tim_Final_029"></a>
  <a href="Tim_Final_030.png"><img class="thumbnail" src="thumbs/Tim_Final_030.png" alt="Tim_Final_030"></a>
  <a href="Tim_Final_031.png"><img class="thumbnail" src="thumbs/Tim_Final_031.png" alt="Tim_Final_031"></a>
  <a href="Tim_Final_032.png"><img class="thumbnail" src="thumbs/Tim_Final_032.png" alt="Tim_Final_032"></a>
  <a href="Tim_Final_033.png"><img class="thumbnail" src="thumbs/Tim_Final_033.png" alt="Tim_Final_033"></a>
  <a href="Tim_Final_034.png"><img class="thumbnail" src="thumbs/Tim_Final_034.png" alt="Tim_Final_034"></a>
  <a href="Tim_Final_035.png"><img class="thumbnail" src="thumbs/Tim_Final_035.png" alt="Tim_Final_035"></a>
  <a href="Tim_Final_036.png"><img class="thumbnail" src="thumbs/Tim_Final_036.png" alt="Tim_Final_036"></a>
  <a href="Tim_Final_037.png"><img class="thumbnail" src="thumbs/Tim_Final_037.png" alt="Tim_Final_037"></a>
  <a href="Tim_Final_038.png"><img class="thumbnail" src="thumbs/Tim_Final_038.png" alt="Tim_Final_038"></a>
  <a href="Tim_Final_039.png"><img class="thumbnail" src="thumbs/Tim_Final_039.png" alt="Tim_Final_039"></a>
  <a href="Tim_Final_040.png"><img class="thumbnail" src="thumbs/Tim_Final_040.png" alt="Tim_Final_040"></a>
  <a href="Tim_Final_041.png"><img class="thumbnail" src="thumbs/Tim_Final_041.png" alt="Tim_Final_041"></a>
  <a href="Tim_Final_042.png"><img class="thumbnail" src="thumbs/Tim_Final_042.png" alt="Tim_Final_042"></a>
  <a href="Tim_Final_043.png"><img class="thumbnail" src="thumbs/Tim_Final_043.png" alt="Tim_Final_043"></a>
  <a href="Tim_Final_044.png"><img class="thumbnail" src="thumbs/Tim_Final_044.png" alt="Tim_Final_044"></a>
  <a href="Tim_Final_045.png"><img class="thumbnail" src="thumbs/Tim_Final_045.png" alt="Tim_Final_045"></a>
  <a href="Tim_Final_046.png"><img class="thumbnail" src="thumbs/Tim_Final_046.png" alt="Tim_Final_046"></a>
  <a href="Tim_Final_047.png"><img class="thumbnail" src="thumbs/Tim_Final_047.png" alt="Tim_Final_047"></a>
  <a href="Tim_Final_048.png"><img class="thumbnail" src="thumbs/Tim_Final_048.png" alt="Tim_Final_048"></a>
  <a href="Tim_Final_049.jpg"><img class="thumbnail" src="thumbs/Tim_Final_049.jpg" alt="Tim_Final_049"></a>
  <a href="Tim_Final_050.jpg"><img class="thumbnail" src="thumbs/Tim_Final_050.jpg" alt="Tim_Final_050"></a>
  <a href="Tim_Final_051.jpg"><img class="thumbnail" src="thumbs/Tim_Final_051.jpg" alt="Tim_Final_051"></a>
</div>
</body>
</html>
