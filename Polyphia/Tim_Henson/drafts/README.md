## AUTHOR

- [Francesco Zubani](https://www.linkedin.com/in/francesco-zubani-5957081a6/)
- [Nicola Ballarini](https://www.instagram.com/odinorunegraf/)

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [Stable Diffusion](https://github.com/CompVis/stable-diffusion) - [LICENSE](https://github.com/CompVis/stable-diffusion/blob/main/LICENSE)
- [ComfyUI](https://github.com/comfyanonymous/ComfyUI) - [LICENSE](https://github.com/comfyanonymous/ComfyUI/blob/master/LICENSE)
- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)

These images are based on the work of Nicola Ballarini and post processed by Francesco Zubani.

## GALLERY

### Tim Henson

<div class="gallery">
  <a href="Tim_Henson_001.png"><img class="thumbnail" src="thumbs/Tim_Henson_001.png" alt="Tim_Henson_001"></a>
  <a href="Tim_Henson_002.png"><img class="thumbnail" src="thumbs/Tim_Henson_002.png" alt="Tim_Henson_002"></a>
  <a href="Tim_Henson_003.png"><img class="thumbnail" src="thumbs/Tim_Henson_003.png" alt="Tim_Henson_003"></a>
  <a href="Tim_Henson_004.png"><img class="thumbnail" src="thumbs/Tim_Henson_004.png" alt="Tim_Henson_004"></a>
  <a href="Tim_Henson_005.png"><img class="thumbnail" src="thumbs/Tim_Henson_005.png" alt="Tim_Henson_005"></a>
  <a href="Tim_Henson_006.png"><img class="thumbnail" src="thumbs/Tim_Henson_006.png" alt="Tim_Henson_006"></a>
  <a href="Tim_Henson_007.png"><img class="thumbnail" src="thumbs/Tim_Henson_007.png" alt="Tim_Henson_007"></a>
  <a href="Tim_Henson_008.png"><img class="thumbnail" src="thumbs/Tim_Henson_008.png" alt="Tim_Henson_008"></a>
  <a href="Tim_Henson_009.png"><img class="thumbnail" src="thumbs/Tim_Henson_009.png" alt="Tim_Henson_009"></a>
  <a href="Tim_Henson_010.png"><img class="thumbnail" src="thumbs/Tim_Henson_010.png" alt="Tim_Henson_010"></a>
  <a href="Tim_Henson_011.png"><img class="thumbnail" src="thumbs/Tim_Henson_011.png" alt="Tim_Henson_011"></a>
  <a href="Tim_Henson_012.png"><img class="thumbnail" src="thumbs/Tim_Henson_012.png" alt="Tim_Henson_012"></a>
  <a href="Tim_Henson_013.png"><img class="thumbnail" src="thumbs/Tim_Henson_013.png" alt="Tim_Henson_013"></a>
  <a href="Tim_Henson_014.png"><img class="thumbnail" src="thumbs/Tim_Henson_014.png" alt="Tim_Henson_014"></a>
  <a href="Tim_Henson_015.png"><img class="thumbnail" src="thumbs/Tim_Henson_015.png" alt="Tim_Henson_015"></a>
  <a href="Tim_Henson_016.png"><img class="thumbnail" src="thumbs/Tim_Henson_016.png" alt="Tim_Henson_016"></a>
  <a href="Tim_Henson_017.png"><img class="thumbnail" src="thumbs/Tim_Henson_017.png" alt="Tim_Henson_017"></a>
  <a href="Tim_Henson_018.png"><img class="thumbnail" src="thumbs/Tim_Henson_018.png" alt="Tim_Henson_018"></a>
  <a href="Tim_Henson_019.png"><img class="thumbnail" src="thumbs/Tim_Henson_019.png" alt="Tim_Henson_019"></a>
  <a href="Tim_Henson_020.png"><img class="thumbnail" src="thumbs/Tim_Henson_020.png" alt="Tim_Henson_020"></a>
  <a href="Tim_Henson_021.png"><img class="thumbnail" src="thumbs/Tim_Henson_021.png" alt="Tim_Henson_021"></a>
  <a href="Tim_Henson_022.png"><img class="thumbnail" src="thumbs/Tim_Henson_022.png" alt="Tim_Henson_022"></a>
  <a href="Tim_Henson_023.png"><img class="thumbnail" src="thumbs/Tim_Henson_023.png" alt="Tim_Henson_023"></a>
  <a href="Tim_Henson_024.png"><img class="thumbnail" src="thumbs/Tim_Henson_024.png" alt="Tim_Henson_024"></a>
  <a href="Tim_Henson_025.png"><img class="thumbnail" src="thumbs/Tim_Henson_025.png" alt="Tim_Henson_025"></a>
  <a href="Tim_Henson_026.png"><img class="thumbnail" src="thumbs/Tim_Henson_026.png" alt="Tim_Henson_026"></a>
  <a href="Tim_Henson_027.png"><img class="thumbnail" src="thumbs/Tim_Henson_027.png" alt="Tim_Henson_027"></a>
  <a href="Tim_Henson_028.png"><img class="thumbnail" src="thumbs/Tim_Henson_028.png" alt="Tim_Henson_028"></a>
  <a href="Tim_Henson_029.png"><img class="thumbnail" src="thumbs/Tim_Henson_029.png" alt="Tim_Henson_029"></a>
  <a href="Tim_Henson_030.png"><img class="thumbnail" src="thumbs/Tim_Henson_030.png" alt="Tim_Henson_030"></a>
  <a href="Tim_Henson_031.png"><img class="thumbnail" src="thumbs/Tim_Henson_031.png" alt="Tim_Henson_031"></a>
  <a href="Tim_Henson_032.png"><img class="thumbnail" src="thumbs/Tim_Henson_032.png" alt="Tim_Henson_032"></a>
  <a href="Tim_Henson_033.png"><img class="thumbnail" src="thumbs/Tim_Henson_033.png" alt="Tim_Henson_033"></a>
  <a href="Tim_Henson_034.png"><img class="thumbnail" src="thumbs/Tim_Henson_034.png" alt="Tim_Henson_034"></a>
  <a href="Tim_Henson_035.png"><img class="thumbnail" src="thumbs/Tim_Henson_035.png" alt="Tim_Henson_035"></a>
  <a href="Tim_Henson_036.png"><img class="thumbnail" src="thumbs/Tim_Henson_036.png" alt="Tim_Henson_036"></a>
  <a href="Tim_Henson_037.png"><img class="thumbnail" src="thumbs/Tim_Henson_037.png" alt="Tim_Henson_037"></a>
  <a href="Tim_Henson_038.png"><img class="thumbnail" src="thumbs/Tim_Henson_038.png" alt="Tim_Henson_038"></a>
  <a href="Tim_Henson_039.png"><img class="thumbnail" src="thumbs/Tim_Henson_039.png" alt="Tim_Henson_039"></a>
  <a href="Tim_Henson_040.png"><img class="thumbnail" src="thumbs/Tim_Henson_040.png" alt="Tim_Henson_040"></a>
  <a href="Tim_Henson_041.png"><img class="thumbnail" src="thumbs/Tim_Henson_041.png" alt="Tim_Henson_041"></a>
  <a href="Tim_Henson_042.png"><img class="thumbnail" src="thumbs/Tim_Henson_042.png" alt="Tim_Henson_042"></a>
  <a href="Tim_Henson_043.png"><img class="thumbnail" src="thumbs/Tim_Henson_043.png" alt="Tim_Henson_043"></a>
  <a href="Tim_Henson_044.png"><img class="thumbnail" src="thumbs/Tim_Henson_044.png" alt="Tim_Henson_044"></a>
  <a href="Tim_Henson_045.png"><img class="thumbnail" src="thumbs/Tim_Henson_045.png" alt="Tim_Henson_045"></a>
  <a href="Tim_Henson_046.png"><img class="thumbnail" src="thumbs/Tim_Henson_046.png" alt="Tim_Henson_046"></a>
  <a href="Tim_Henson_047.png"><img class="thumbnail" src="thumbs/Tim_Henson_047.png" alt="Tim_Henson_047"></a>
  <a href="Tim_Henson_048.png"><img class="thumbnail" src="thumbs/Tim_Henson_048.png" alt="Tim_Henson_048"></a>
  <a href="Tim_Henson_049.png"><img class="thumbnail" src="thumbs/Tim_Henson_049.png" alt="Tim_Henson_049"></a>
  <a href="Tim_Henson_050.png"><img class="thumbnail" src="thumbs/Tim_Henson_050.png" alt="Tim_Henson_050"></a>
  <a href="Tim_Henson_051.png"><img class="thumbnail" src="thumbs/Tim_Henson_051.png" alt="Tim_Henson_051"></a>
  <a href="Tim_Henson_052.png"><img class="thumbnail" src="thumbs/Tim_Henson_052.png" alt="Tim_Henson_052"></a>
  <a href="Tim_Henson_053.png"><img class="thumbnail" src="thumbs/Tim_Henson_053.png" alt="Tim_Henson_053"></a>
  <a href="Tim_Henson_054.png"><img class="thumbnail" src="thumbs/Tim_Henson_054.png" alt="Tim_Henson_054"></a>
  <a href="Tim_Henson_055.png"><img class="thumbnail" src="thumbs/Tim_Henson_055.png" alt="Tim_Henson_055"></a>
  <a href="Tim_Henson_056.png"><img class="thumbnail" src="thumbs/Tim_Henson_056.png" alt="Tim_Henson_056"></a>
  <a href="Tim_Henson_057.png"><img class="thumbnail" src="thumbs/Tim_Henson_057.png" alt="Tim_Henson_057"></a>
  <a href="Tim_Henson_058.png"><img class="thumbnail" src="thumbs/Tim_Henson_058.png" alt="Tim_Henson_058"></a>
  <a href="Tim_Henson_059.png"><img class="thumbnail" src="thumbs/Tim_Henson_059.png" alt="Tim_Henson_059"></a>
  <a href="Tim_Henson_060.png"><img class="thumbnail" src="thumbs/Tim_Henson_060.png" alt="Tim_Henson_060"></a>
  <a href="Tim_Henson_061.png"><img class="thumbnail" src="thumbs/Tim_Henson_061.png" alt="Tim_Henson_061"></a>
  <a href="Tim_Henson_062.png"><img class="thumbnail" src="thumbs/Tim_Henson_062.png" alt="Tim_Henson_062"></a>
  <a href="Tim_Henson_063.png"><img class="thumbnail" src="thumbs/Tim_Henson_063.png" alt="Tim_Henson_063"></a>
  <a href="Tim_Henson_064.png"><img class="thumbnail" src="thumbs/Tim_Henson_064.png" alt="Tim_Henson_064"></a>
  <a href="Tim_Henson_065.png"><img class="thumbnail" src="thumbs/Tim_Henson_065.png" alt="Tim_Henson_065"></a>
  <a href="Tim_Henson_066.png"><img class="thumbnail" src="thumbs/Tim_Henson_066.png" alt="Tim_Henson_066"></a>
  <a href="Tim_Henson_067.png"><img class="thumbnail" src="thumbs/Tim_Henson_067.png" alt="Tim_Henson_067"></a>
  <a href="Tim_Henson_068.png"><img class="thumbnail" src="thumbs/Tim_Henson_068.png" alt="Tim_Henson_068"></a>
  <a href="Tim_Henson_069.png"><img class="thumbnail" src="thumbs/Tim_Henson_069.png" alt="Tim_Henson_069"></a>
  <a href="Tim_Henson_070.png"><img class="thumbnail" src="thumbs/Tim_Henson_070.png" alt="Tim_Henson_070"></a>
  <a href="Tim_Henson_071.png"><img class="thumbnail" src="thumbs/Tim_Henson_071.png" alt="Tim_Henson_071"></a>
  <a href="Tim_Henson_072.png"><img class="thumbnail" src="thumbs/Tim_Henson_072.png" alt="Tim_Henson_072"></a>
  <a href="Tim_Henson_073.png"><img class="thumbnail" src="thumbs/Tim_Henson_073.png" alt="Tim_Henson_073"></a>
  <a href="Tim_Henson_074.png"><img class="thumbnail" src="thumbs/Tim_Henson_074.png" alt="Tim_Henson_074"></a>
  <a href="Tim_Henson_075.png"><img class="thumbnail" src="thumbs/Tim_Henson_075.png" alt="Tim_Henson_075"></a>
  <a href="Tim_Henson_076.png"><img class="thumbnail" src="thumbs/Tim_Henson_076.png" alt="Tim_Henson_076"></a>
  <a href="Tim_Henson_077.png"><img class="thumbnail" src="thumbs/Tim_Henson_077.png" alt="Tim_Henson_077"></a>
  <a href="Tim_Henson_078.png"><img class="thumbnail" src="thumbs/Tim_Henson_078.png" alt="Tim_Henson_078"></a>
  <a href="Tim_Henson_079.png"><img class="thumbnail" src="thumbs/Tim_Henson_079.png" alt="Tim_Henson_079"></a>
  <a href="Tim_Henson_080.png"><img class="thumbnail" src="thumbs/Tim_Henson_080.png" alt="Tim_Henson_080"></a>
  <a href="Tim_Henson_081.png"><img class="thumbnail" src="thumbs/Tim_Henson_081.png" alt="Tim_Henson_081"></a>
  <a href="Tim_Henson_082.png"><img class="thumbnail" src="thumbs/Tim_Henson_082.png" alt="Tim_Henson_082"></a>
  <a href="Tim_Henson_083.png"><img class="thumbnail" src="thumbs/Tim_Henson_083.png" alt="Tim_Henson_083"></a>
  <a href="Tim_Henson_084.png"><img class="thumbnail" src="thumbs/Tim_Henson_084.png" alt="Tim_Henson_084"></a>
  <a href="Tim_Henson_085.png"><img class="thumbnail" src="thumbs/Tim_Henson_085.png" alt="Tim_Henson_085"></a>
  <a href="Tim_Henson_086.png"><img class="thumbnail" src="thumbs/Tim_Henson_086.png" alt="Tim_Henson_086"></a>
  <a href="Tim_Henson_087.png"><img class="thumbnail" src="thumbs/Tim_Henson_087.png" alt="Tim_Henson_087"></a>
  <a href="Tim_Henson_088.png"><img class="thumbnail" src="thumbs/Tim_Henson_088.png" alt="Tim_Henson_088"></a>
  <a href="Tim_Henson_089.png"><img class="thumbnail" src="thumbs/Tim_Henson_089.png" alt="Tim_Henson_089"></a>
  <a href="Tim_Henson_090.png"><img class="thumbnail" src="thumbs/Tim_Henson_090.png" alt="Tim_Henson_090"></a>
  <a href="Tim_Henson_091.png"><img class="thumbnail" src="thumbs/Tim_Henson_091.png" alt="Tim_Henson_091"></a>
  <a href="Tim_Henson_092.png"><img class="thumbnail" src="thumbs/Tim_Henson_092.png" alt="Tim_Henson_092"></a>
  <a href="Tim_Henson_093.png"><img class="thumbnail" src="thumbs/Tim_Henson_093.png" alt="Tim_Henson_093"></a>
  <a href="Tim_Henson_094.png"><img class="thumbnail" src="thumbs/Tim_Henson_094.png" alt="Tim_Henson_094"></a>
  <a href="Tim_Henson_095.png"><img class="thumbnail" src="thumbs/Tim_Henson_095.png" alt="Tim_Henson_095"></a>
  <a href="Tim_Henson_096.png"><img class="thumbnail" src="thumbs/Tim_Henson_096.png" alt="Tim_Henson_096"></a>
  <a href="Tim_Henson_097.png"><img class="thumbnail" src="thumbs/Tim_Henson_097.png" alt="Tim_Henson_097"></a>
  <a href="Tim_Henson_098.png"><img class="thumbnail" src="thumbs/Tim_Henson_098.png" alt="Tim_Henson_098"></a>
  <a href="Tim_Henson_099.png"><img class="thumbnail" src="thumbs/Tim_Henson_099.png" alt="Tim_Henson_099"></a>
  <a href="Tim_Henson_100.png"><img class="thumbnail" src="thumbs/Tim_Henson_100.png" alt="Tim_Henson_100"></a>
  <a href="Tim_Henson_101.png"><img class="thumbnail" src="thumbs/Tim_Henson_101.png" alt="Tim_Henson_101"></a>
  <a href="Tim_Henson_102.png"><img class="thumbnail" src="thumbs/Tim_Henson_102.png" alt="Tim_Henson_102"></a>
  <a href="Tim_Henson_103.png"><img class="thumbnail" src="thumbs/Tim_Henson_103.png" alt="Tim_Henson_103"></a>
  <a href="Tim_Henson_104.png"><img class="thumbnail" src="thumbs/Tim_Henson_104.png" alt="Tim_Henson_104"></a>
  <a href="Tim_Henson_105.png"><img class="thumbnail" src="thumbs/Tim_Henson_105.png" alt="Tim_Henson_105"></a>
  <a href="Tim_Henson_106.png"><img class="thumbnail" src="thumbs/Tim_Henson_106.png" alt="Tim_Henson_106"></a>
  <a href="Tim_Henson_107.png"><img class="thumbnail" src="thumbs/Tim_Henson_107.png" alt="Tim_Henson_107"></a>
</div>
</body>
</html>
